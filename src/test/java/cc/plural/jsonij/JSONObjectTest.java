/*
 * Copyright 2011 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import static org.junit.Assert.*;

/**
 * JSON.Object Type Tests
 * 
 * @author jmarsden@plural.cc
 */
public class JSONObjectTest {

    public JSONObjectTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testConstruct() {
        System.out.println("construct");
        JSON.Object<String, Value> value1 = new JSON.Object<String, Value>();
        value1.put("attribute1", JSON.NULL);
        value1.put("attribute2", JSON.TRUE);
        value1.put("attribute3", JSON.FALSE);
        value1.put("attribute4", new JSON.Numeric(33.0));
        value1.put("attribute5", new JSON.String("I am a String"));
        value1.put("attribute6", new JSON.Object<String, Value>());
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        JSON.Object<String, Value> value1 = new JSON.Object<String, Value>();
        value1.put("attribute1", JSON.NULL);
        value1.put("attribute2", JSON.TRUE);
        value1.put("attribute3", JSON.FALSE);
        value1.put("attribute4", new JSON.Numeric(33.0));
        value1.put("attribute5", new JSON.String("I am a String"));
        value1.put("attribute6", new JSON.Object<String, Value>());

        JSON.Object<String, Value> value2 = new JSON.Object<String, Value>();
        value2.put("attribute6", new JSON.Object<String, Value>());
        value2.put("attribute5", new JSON.String("I am a String"));
        value2.put("attribute4", new JSON.Numeric(33.0));
        value2.put("attribute1", JSON.NULL);
        value2.put("attribute2", JSON.TRUE);
        value2.put("attribute3", JSON.FALSE);

        assertEquals(value1, value2);
    }

    @Test
    public void testToJSON() {
        System.out.println("toJSON");
        JSON.Object<String, Value> value1 = new JSON.Object<String, Value>();
        value1.put("attribute1", JSON.NULL);
        value1.put("attribute2", JSON.TRUE);
        value1.put("attribute3", JSON.FALSE);
        value1.put("attribute4", new JSON.Numeric(33.0));
        value1.put("attribute5", new JSON.String("I am a String"));
        value1.put("attribute6", new JSON.Object<String, Value>());
        System.out.println(value1.toJSON());
        assertEquals(value1.toJSON(), "{\"attribute1\":null,\"attribute2\":true,\"attribute3\":false,\"attribute4\":33.0,\"attribute5\":\"I am a String\",\"attribute6\":{}}");
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        JSON.Object<String, Value> value1 = new JSON.Object<String, Value>();
        value1.put("attribute1", JSON.NULL);
        value1.put("attribute2", JSON.TRUE);
        value1.put("attribute3", JSON.FALSE);
        value1.put("attribute4", new JSON.Numeric(33.0));
        value1.put("attribute5", new JSON.String("I am a String"));
        value1.put("attribute6", new JSON.Object<String, Value>());
        System.out.println(value1.toString());
        assertEquals(value1.toString(), "{\"attribute1\":null,\"attribute2\":true,\"attribute3\":false,\"attribute4\":33.0,\"attribute5\":\"I am a String\",\"attribute6\":{}}");
    }

    @Test
    public void testGet() {
        System.out.println("get");
        JSON.Object<String, Value> value1 = new JSON.Object<String, Value>();
        value1.put("attribute1", JSON.NULL);
        value1.put("attribute2", JSON.TRUE);
        value1.put("attribute3", JSON.FALSE);
        value1.put("attribute4", new JSON.Numeric(33.0));
        value1.put("attribute5", new JSON.String("I am a String"));
        value1.put("attribute6", new JSON.Object<String, Value>());

        assertEquals(value1.get("attribute3"), JSON.FALSE);
        assertEquals(value1.get(new JSON.String("attribute3")), JSON.FALSE);
        
        JSON.Object<JSON.String, Value> value2 = new JSON.Object<JSON.String, Value>();
        value2.put(new JSON.String("attribute6"), new JSON.Object<String, Value>());
        value2.put(new JSON.String("attribute5"), new JSON.String("I am a String"));
        value2.put(new JSON.String("attribute4"), new JSON.Numeric(33.0));
        value2.put(new JSON.String("attribute1"), JSON.NULL);
        value2.put(new JSON.String("attribute2"), JSON.TRUE);
        value2.put(new JSON.String("attribute3"), JSON.FALSE);
        
        assertEquals(value2.get(new JSON.String("attribute5")), "I am a String");
        assertEquals(value2.get("attribute5"), new JSON.String("I am a String"));
        
        assertEquals(value1, value2);
    }
}