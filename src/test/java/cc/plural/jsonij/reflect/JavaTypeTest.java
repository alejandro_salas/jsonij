/*
 * Copyright 2011 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.reflect;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import cc.plural.jsonij.reflect.ReflectType;
import static org.junit.Assert.*;

/**
 *
 * @author jmarsden@plural.cc
 */
public class JavaTypeTest {

    public JavaTypeTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * Test of setPrimitive method, of class JavaType.
     */
    @Test
    public void testSetPrimitive() {
        System.out.println("setPrimitive");
        boolean primitive = false;
        ReflectType instance = ReflectType.INTEGER;
        instance.setPrimitive(primitive);
        assertEquals(instance.isPrimitive(), primitive);
    }

    /**
     * Test of isPrimitive method, of class JavaType.
     */
    @Test
    public void testIsPrimitive() {
        System.out.println("isPrimitive");
        boolean primitive = true;
        ReflectType instance = ReflectType.INTEGER;
        instance.setPrimitive(primitive);
        assertEquals(instance.isPrimitive(), primitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void inspectObjectType_boolean() {
        System.out.println("inspectObjectType_boolean");
        Class<?> c = null;
        ReflectType expResult = null;

        c = boolean.class;
        expResult = ReflectType.BOOLEAN;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Boolean() {
        System.out.println("inspectObjectType_Boolean");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Boolean.class;
        expResult = ReflectType.BOOLEAN;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void inspectObjectType_byte() {
        System.out.println("inspectObjectType_byte");
        Class<?> c = null;
        ReflectType expResult = null;

        c = byte.class;
        expResult = ReflectType.BYTE;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Byte() {
        System.out.println("inspectObjectType_Byte");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Byte.class;
        expResult = ReflectType.BYTE;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void inspectObjectType_short() {
        System.out.println("inspectObjectType_short");
        Class<?> c = null;
        ReflectType expResult = null;

        c = short.class;
        expResult = ReflectType.SHORT;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Short() {
        System.out.println("inspectObjectType_Short");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Short.class;
        expResult = ReflectType.SHORT;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_int() {
        System.out.println("inspectObjectType_int");
        Class<?> c = null;
        ReflectType expResult = null;

        c = int.class;
        expResult = ReflectType.INTEGER;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Integer() {
        System.out.println("inspectObjectType_Integer");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Integer.class;
        expResult = ReflectType.INTEGER;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_float() {
        System.out.println("inspectObjectType_float");
        Class<?> c = null;
        ReflectType expResult = null;

        c = float.class;
        expResult = ReflectType.FLOAT;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Float() {
        System.out.println("inspectObjectType_Integer");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Float.class;
        expResult = ReflectType.FLOAT;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_long() {
        System.out.println("inspectObjectType_long");
        Class<?> c = null;
        ReflectType expResult = null;

        c = long.class;
        expResult = ReflectType.LONG;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Long() {
        System.out.println("inspectObjectType_Long");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Long.class;
        expResult = ReflectType.LONG;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_double() {
        System.out.println("inspectObjectType_double");
        Class<?> c = null;
        ReflectType expResult = null;

        c = double.class;
        expResult = ReflectType.DOUBLE;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Double() {
        System.out.println("inspectObjectType_Double");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Double.class;
        expResult = ReflectType.DOUBLE;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_String() {
        System.out.println("inspectObjectType_String");
        Class<?> c = null;
        ReflectType expResult = null;

        c = String.class;
        expResult = ReflectType.STRING;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_List() {
        System.out.println("inspectObjectType_List");
        Class<?> c = null;
        ReflectType expResult = null;

        c = List.class;
        expResult = ReflectType.LIST;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_ArrayList() {
        System.out.println("inspectObjectType_ArrayList");
        Class<?> c = null;
        ReflectType expResult = null;

        c = ArrayList.class;
        expResult = ReflectType.LIST;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);
    }
    
    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Map() {
        System.out.println("inspectObjectType_Map");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Map.class;
        expResult = ReflectType.MAP;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);
    }
    
    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_HashMap() {
        System.out.println("inspectObjectType_HashMap");
        Class<?> c = null;
        ReflectType expResult = null;

        c = HashMap.class;
        expResult = ReflectType.MAP;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);
    }
    
    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_HashDate() {
        System.out.println("inspectObjectType_HashDate");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Date.class;
        expResult = ReflectType.OBJECT;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);
    }
    
    /**
     * Test thats all in the name.
     */
    @Test
    public void inspectObjectType_boolean_Array() {
        System.out.println("inspectObjectType_boolean_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = boolean[].class;
        expResult = ReflectType.ARRAY_BOOLEAN;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Boolean_Array() {
        System.out.println("inspectObjectType_Boolean_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Boolean[].class;
        expResult = ReflectType.ARRAY_BOOLEAN;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }
    
        /**
     * Test thats all in the name.
     */
    @Test
    public void inspectObjectType_byte_Array() {
        System.out.println("inspectObjectType_byte_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = byte[].class;
        expResult = ReflectType.ARRAY_BYTE;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Byte_Array() {
        System.out.println("inspectObjectType_Byte_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Byte[].class;
        expResult = ReflectType.ARRAY_BYTE;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void inspectObjectType_short_Array() {
        System.out.println("inspectObjectType_short_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = short[].class;
        expResult = ReflectType.ARRAY_SHORT;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Short_Array() {
        System.out.println("inspectObjectType_Short_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Short[].class;
        expResult = ReflectType.ARRAY_SHORT;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_int_Array() {
        System.out.println("inspectObjectType_int");
        Class<?> c = null;
        ReflectType expResult = null;

        c = int[].class;
        expResult = ReflectType.ARRAY_INTEGER;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Integer_Array() {
        System.out.println("inspectObjectType_Integer");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Integer[].class;
        expResult = ReflectType.ARRAY_INTEGER;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }
    
    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_float_Array() {
        System.out.println("inspectObjectType_float_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = float[].class;
        expResult = ReflectType.ARRAY_FLOAT;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Float_Array() {
        System.out.println("inspectObjectType_Integer_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Float[].class;
        expResult = ReflectType.ARRAY_FLOAT;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_long_Array() {
        System.out.println("inspectObjectType_long_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = long[].class;
        expResult = ReflectType.ARRAY_LONG;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Long_Array() {
        System.out.println("inspectObjectType_Long_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Long[].class;
        expResult = ReflectType.ARRAY_LONG;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_double_Array() {
        System.out.println("inspectObjectType_double_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = double[].class;
        expResult = ReflectType.ARRAY_DOUBLE;

        ReflectType result = ReflectType.inspectObjectType(c);
        System.out.println(String.format("Found Class: \"%s\"", c));
        assertEquals(expResult, result);

        boolean expResultPrimitive = true;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_Double_Array() {
        System.out.println("inspectObjectType_Double_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = Double[].class;
        expResult = ReflectType.ARRAY_DOUBLE;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);

        boolean expResultPrimitive = false;
        boolean resultPrimitive = result.isPrimitive();
        assertEquals(expResultPrimitive, resultPrimitive);
    }

    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_String_Array() {
        System.out.println("inspectObjectType_String_Array");
        Class<?> c = null;
        ReflectType expResult = null;

        c = String[].class;
        expResult = ReflectType.ARRAY_STRING;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);
    }
    
    /**
     * Test thats all in the name.
     */
    @Test
    public void testInspectObjectType_int_2DArray() {
        System.out.println("inspectObjectType_int_2DArray");
        Class<?> c = null;
        ReflectType expResult = null;

        c = int[][].class;
        expResult = ReflectType.ARRAY_ARRAY;

        System.out.println(String.format("Found Class: \"%s\"", c));
        ReflectType result = ReflectType.inspectObjectType(c);
        assertEquals(expResult, result);
    }
}
