/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.jsonij.reflect;

import cc.plural.jsonij.reflect.helpers.TestClass2;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.BeforeClass;

/**
 *
 * @author jmarsden
 */
public class InspectionFactoryTest {

    public InspectionFactoryTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of inspect method, of class InspectionFactory.
     */
    @Test
    public void testInspect_Class() {
        System.out.println("testInspect_Class");
        Class<?> klass = TestClass2.class;
        InspectionFactory instance = new InspectionFactory();

        Inspection result = instance.inspect(klass);
        
        System.out.println(result);
        for(ClassProperty prop : result.getProperties()) {
            System.out.println(prop);
        }
    }
}
