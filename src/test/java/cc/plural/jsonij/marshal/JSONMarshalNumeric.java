/*
 * Copyright 2012 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.marshal;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.parser.ParserException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jmarsden
 */
public class JSONMarshalNumeric {
    
    public JSONMarshalNumeric() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testMarshalJSONNumericByte() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testMarshalJSONNumericByte");

        String inputJSONDocument = "1";        
        Value inputValue = JSON.parseValue(inputJSONDocument);
        System.out.println(String.format("InputJSON: %s", inputValue.toJSON()));
        Object marshal = JSONMarshaler.marshalJSON(inputValue, byte.class);
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        Value outputValue = JSONMarshaler.marshalObjectToValue(marshal);
        System.out.println(String.format("OutputJSON: %s", outputValue));

        assertNotNull(marshal);
        assertEquals(inputValue, outputValue);
    }
    
    @Test
    public void testMarshalJSONNumericFloat() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testMarshalJSONNumericFloat");

        String inputJSONDocument = "-33333";        
        Value inputValue = JSON.parseValue(inputJSONDocument);
        System.out.println(String.format("InputJSON: %s", inputValue.toJSON()));
        Object marshal = JSONMarshaler.marshalJSON(inputValue, Float.class);
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        Value outputValue = JSONMarshaler.marshalObjectToValue(marshal);
        System.out.println(String.format("OutputJSON: %s", outputValue));

        assertNotNull(marshal);
        assertEquals(inputValue, outputValue);
    }
    
    @Test
    public void testMarshalJSONNumericInteger() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testMarshalJSONNumericInteger");

        String inputJSONDocument = "1337";        
        Value inputValue = JSON.parseValue(inputJSONDocument);
        System.out.println(String.format("InputJSON: %s", inputValue.toJSON()));
        Object marshal = JSONMarshaler.marshalJSON(inputValue, int.class);
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        Value outputValue = JSONMarshaler.marshalObjectToValue(marshal);
        System.out.println(String.format("OutputJSON: %s", outputValue));

        assertNotNull(marshal);
        assertEquals(inputValue, outputValue);
    }
    
    @Test
    public void testMarshalJSONNumericBigInteger() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testMarshalJSONNumericBigInteger");

        String inputJSONDocument = "12345678900987654321123456789009876543211234567890098765432112345678900987654321";        
        Value inputValue = JSON.parseValue(inputJSONDocument);
        System.out.println(String.format("InputJSON: %s", inputValue.toJSON()));
        Object marshal = JSONMarshaler.marshalJSON(inputValue, BigInteger.class);
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        Value outputValue = JSONMarshaler.marshalObjectToValue(marshal);
        System.out.println(String.format("OutputJSON: %s", outputValue));

        assertNotNull(marshal);
        assertEquals(inputValue, outputValue);
    }
    
    @Test
    public void testMarshalJSONNumericBigDecimal() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testMarshalJSONNumericBigDecimal");

        String inputJSONDocument = "12345678900987654321123456789009876543211234567890098765432112345678900987654321.12345678900987654321123456789009876543211234567890098765432112345678900987654321123456789009";        
        Value inputValue = JSON.parseValue(inputJSONDocument);
        System.out.println(String.format("InputJSON: %s", inputValue.toJSON()));
        Object marshal = JSONMarshaler.marshalJSON(inputValue, BigDecimal.class);
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        Value outputValue = JSONMarshaler.marshalObjectToValue(marshal);
        System.out.println(String.format("OutputJSON: %s", outputValue));

        assertNotNull(marshal);
        assertEquals(inputValue, outputValue);
    }
}
