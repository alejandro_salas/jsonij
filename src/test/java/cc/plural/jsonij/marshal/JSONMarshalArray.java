/*
 * Copyright 2012 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.marshal;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.parser.ParserException;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jmarsden
 */
public class JSONMarshalArray {

    public JSONMarshalArray() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMarshalJSONArray() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testMarshalJSONArray");
        String inputJSONDocument = "[]";
        Value inputValue = JSON.parseValue(inputJSONDocument);
        System.out.println(String.format("InputJSON: %s", inputValue.toJSON()));
        Object marshal = JSONMarshaler.marshalJSON(inputValue, Object[].class);
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        Value outputValue = JSONMarshaler.marshalObjectToValue(marshal);
        System.out.println(String.format("OutputJSON: %s", outputValue));

        assertNotNull(marshal);
        assertEquals(inputValue, outputValue);
    }

    @Test
    public void testMarshalJSONArrayBoolean() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testMarshalJSONArrayBoolean");
        String inputJSONDocument = "[true, false, true, true, true, false]";
        Value inputValue = JSON.parseValue(inputJSONDocument);
        System.out.println(String.format("InputJSON: %s", inputValue.toJSON()));
        Object marshal = JSONMarshaler.marshalJSON(inputValue, boolean[].class);
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        Value outputValue = JSONMarshaler.marshalObjectToValue(marshal);
        System.out.println(String.format("OutputJSON: %s", outputValue));

        assertNotNull(marshal);
        assertEquals(inputValue, outputValue);
    }
    
    @Test
    public void testMarshalJSONArrayInt() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testMarshalJSONArrayInt");
        String inputJSONDocument = "[1, 2, 3, -1000, 0, -55]";
        Value inputValue = JSON.parseValue(inputJSONDocument);
        System.out.println(String.format("InputJSON: %s", inputValue.toJSON()));
        Object marshal = JSONMarshaler.marshalJSON(inputValue, int[].class);
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        Value outputValue = JSONMarshaler.marshalObjectToValue(marshal);
        System.out.println(String.format("OutputJSON: %s", outputValue));
        assertNotNull(marshal);
        assertEquals(inputValue, outputValue);
    }
    
    @Test
    public void testMarshalJSONArrayString() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testMarshalJSONArrayString");
        String inputJSONDocument = "[\"\",\"Rah\",\"Blah Blah Blah Blah\"]";
        Value inputValue = JSON.parseValue(inputJSONDocument);
        System.out.println(String.format("InputJSON: %s", inputValue.toJSON()));
        Object marshal = JSONMarshaler.marshalJSON(inputValue, String[].class);
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        Value outputValue = JSONMarshaler.marshalObjectToValue(marshal);
        System.out.println(String.format("OutputJSON: %s", outputValue));
        assertNotNull(marshal);
        assertEquals(inputValue, outputValue);
    }
    
    @Test
    public void testMarshalJSONArrayCharSequence() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testMarshalJSONArrayCharSequence");
        String inputJSONDocument = "[\"\",\"Rah\",\"Blah Blah Blah Blah\"]";
        Value inputValue = JSON.parseValue(inputJSONDocument);
        System.out.println(String.format("InputJSON: %s", inputValue.toJSON()));
        Object marshal = JSONMarshaler.marshalJSON(inputValue, CharSequence[].class);
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        Value outputValue = JSONMarshaler.marshalObjectToValue(marshal);
        System.out.println(String.format("OutputJSON: %s", outputValue));
        assertNotNull(marshal);
        assertEquals(inputValue, outputValue);
    }
    
    @Test
    public void testMarshalJSONArrayArray() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testMarshalJSONArrayCharSequence");
        String inputJSONDocument = "[\"blah\",[1,2,3,4]]";
        Value inputValue = JSON.parseValue(inputJSONDocument);
        System.out.println(String.format("InputJSON: %s", inputValue.toJSON()));
        Object marshal = JSONMarshaler.marshalJSON(inputValue, Object[].class);
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        Value outputValue = JSONMarshaler.marshalObjectToValue(marshal);
        System.out.println(String.format("OutputJSON: %s", outputValue));
        assertNotNull(marshal);
        assertEquals(inputValue, outputValue);
    }
}
