/*
 * Copyright 2012 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.marshal;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.TypeVariable;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.marshal.helpers.ObjectWithMapObjectObject;
import cc.plural.jsonij.parser.ParserException;
import org.junit.Test;

/**
 *
 * @author jmarsden
 */
public class ObjectWithMapPropetyMarshalTests {

    @Test
    public void testReflect() throws ParserException, IOException, NoSuchMethodException, NoSuchFieldException {
        Class<?> clazz = ObjectWithMapObjectObject.class;
        Method method = clazz.getMethod("getMap");
        java.lang.reflect.Type returnType = method.getGenericReturnType();
        
        TypeVariable<Method>[] typeArray = method.getTypeParameters();
        
        Field field = clazz.getField("map");
        java.lang.reflect.Type type = field.getGenericType();  
    }
    
    @Test
    public void testObjectMapProperty() throws ParserException, IOException {
        System.out.println("testObjectMapProperty");
        
        String jsonString = "{\"map\":{\"1\":\"One\",\"2\":\"Two\"}}";
        JSON json = JSON.parse(jsonString);
        
        //ObjectWithMapObjectObject map = (ObjectWithMapObjectObject) JSONMarshaler.marshalJSON(json, ObjectWithMapObjectObject.class);
        
        //assertEquals(map.getMap().size(), 2);
    }
}
