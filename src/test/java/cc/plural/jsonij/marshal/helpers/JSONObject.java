package cc.plural.jsonij.marshal.helpers;

public class JSONObject {

    public int rah;
    CharSequence message;
    public long id;
    private Boolean flag;
    public JSONObject2 inner;
    public double[] innerDoubleArray;
    
    public JSONObject() {
        rah = -1;
        message = null;
        id = -1;
        flag = false;
    }

    public int getRah() {
        return rah;
    }

    public void setRah(int rah) {
        this.rah = rah;
    }

    public CharSequence getMessage() {
        return message;
    }

    public void setMessage(CharSequence message) {
        this.message = message;
    }
    
    public void setFlag(Boolean flag) {
        this.flag = flag;
    }
    
    public Boolean getFlag() {
        return flag;
    }
    
    @Override
    public String toString() {
        return String.format("JSONObject[%s,%s,%s,%s,%s]", id, rah, message, flag, inner);
    }
}
