/*
 *  Copyright 2011 jmarsden.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package jsonij.legacy.marshal;

import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.JSONMarshaler;
import cc.plural.jsonij.marshal.JSONMarshalerException;
import cc.plural.jsonij.marshal.JavaMarshaler;
import cc.plural.jsonij.marshal.JavaMarshalerObjects;
import org.junit.Test;

/**
 *
 * @author jmarsden
 */
public class Cycle {

    @Test
    public void testCycle() throws JSONMarshalerException {
        System.out.println("testCycle");

        Book book = new Book("John Marsden", "Tomorrow When The War Began");
        Owner owner = new Owner("The Other John Marsden");
        book.setOwner(owner);
        owner.setBook(book);

        System.out.println("0 nested levels");
        JavaMarshaler marshaler = new JavaMarshaler();
        Value output = marshaler.marshalObject(book);
        System.out.println(output.toJSON());

        output = marshaler.marshalObject(owner);
        System.out.println(output.toJSON());

        System.out.println("1 nested level");
        JSONMarshaler.setCycleLevels(1);
        marshaler = new JavaMarshaler();
        output = marshaler.marshalObject(book);
        System.out.println(output.toJSON());

        output = marshaler.marshalObject(owner);
        System.out.println(output.toJSON());

        System.out.println("2 nested levels");
        JSONMarshaler.setCycleLevels(2);
        marshaler = new JavaMarshaler();
        output = marshaler.marshalObject(book);
        System.out.println(output.toJSON());

        output = marshaler.marshalObject(owner);
        System.out.println(output.toJSON());
    }

    public static class Owner {

        String name;
        Book book;

        public Owner(String name) {
            this.name = name;
        }

        public Book getBook() {
            return book;
        }

        public void setBook(Book book) {
            this.book = book;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class Book {

        String author;
        String title;
        Owner owner;

        public Book() {
            this(null, null);
        }

        public Book(String author, String title) {
            this.author = author;
            this.title = title;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Owner getOwner() {
            return owner;
        }

        public void setOwner(Owner owner) {
            this.owner = owner;
        }
    }
}
