/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package jsonij.legacy;

import java.io.IOException;
import java.util.Locale;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.ThreadSafeJSONParser;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.parser.ParserException;

import junit.framework.TestCase;

public class JSONParserTest extends TestCase {

    ThreadSafeJSONParser parser;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        parser = new ThreadSafeJSONParser();
        parser.setLocale(Locale.ENGLISH);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        parser = null;
    }

    @SuppressWarnings("unused")
    public void testEmptyString() throws IOException, ParserException {
        try {
            Value v = parser.parse("");
            fail();
        } catch (Exception e) {
        }
    }

    public void testParseObject() throws IOException, ParserException {
        Value v = parser.parse("{}");

        assertTrue(v.isNull());

        v = parser.parse("{\"name\":\n1}");

        assertEquals(v.get("name").getInt(), 1);

        v = parser.parse("{\"name\":\n1.2}");

        assertEquals(v.get("name").getDouble(), 1.2);

        v = parser.parse("{\"name\":\n[1]}");

        assertEquals(v.get("name").get(0).getInt(), 1);

        v = parser.parse("{\"name\":\n\"hello\"}");

        assertEquals(v.get("name").toString(), "hello");

        v = parser.parse("{}");
        
        assertEquals(v.toJSON(), "{}");
    }

    public void testParseArray() throws IOException, ParserException {
        Value v = parser.parse("[]");
        if (!(v instanceof JSON.Array<?>)) {
            fail();
        }
        JSON.Array<JSON.String> vArray = (JSON.Array<JSON.String>) v;
        if (vArray.size() != 0) {
            fail();
        }
        v = parser.parse(" [  ] ");
        if (!(v instanceof JSON.Array<?>)) {
            fail();
        }
        vArray = (JSON.Array<JSON.String>) v;
        if (vArray.size() != 0) {
            fail();
        }
        v = parser.parse(" [true]");
        if (!(v instanceof JSON.Array<?>)) {
            fail();
        }
        vArray = (JSON.Array<JSON.String>) v;
        if (vArray.size() != 1) {
            fail();
        }
        v = vArray.get(0);
        v = parser.parse("[true,1] ");
        if (!(v instanceof JSON.Array<?>)) {
            fail();
        }
        v = parser.parse(" [    \t     true, \n 1.0] ");
        if (!(v instanceof JSON.Array<?>)) {
            fail();
        }
    }

    public void testParseString() throws IOException, ParserException {
        Value v = parser.parse(" [ \n1.20, 234, null, \"John\",true,    \n\n\n\t     - 33, 234E-34, 0.23E23  ,[33, true],   false    ]");
        System.out.println("" + v.nestedSize() + ": " + v.toJSON());
        v = parser.parse("[1.2, 234, null, \"John\", true, -33, 2.34E-32, 0.23, [33, true], false]");
        System.out.println("" + v.nestedSize() + ": " + v.toJSON());
        v = parser.parse("{\"name\":\"John\", \"data\": [1,2,3,4,5,6]}");
        System.out.println("" + v.nestedSize() + ": " + v.toJSON());
        v = parser.parse("{\"na$me\":\"John\",                          \"blah\": {\"silly\": [[[2, [[[[[[[true]]]]]]]]], -55]}}");
        System.out.println("" + v.nestedSize() + ": " + v.toJSON());
    }

    public void testParseNumeric() throws IOException, ParserException {
        parser.parse("[1,2.3,0.4,-5,-5.92,0.001E1,-0.045E45,987654321]");
    }
}
