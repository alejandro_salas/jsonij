/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package jsonij.legacy;

import java.util.Locale;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.ThreadSafeJSONParser;
import cc.plural.jsonij.Value;

/**
 *
 * @author jmarsden
 */
public class JSONConstructTest {

    ThreadSafeJSONParser parser;

    public JSONConstructTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        parser = new ThreadSafeJSONParser();
        parser.setLocale(Locale.ENGLISH);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCreateJSONObject() throws Exception {
        System.out.println("testCreateJSONObject");
        JSON.Object<JSON.String, Value> object = new JSON.Object<JSON.String, Value>();
        object.put(new JSON.String("attribute1"), new JSON.Numeric(33));
        object.put(new JSON.String("attribute2"), new JSON.String("value"));
        JSON jsonInstance = new JSON(object);
        System.out.println(jsonInstance.toJSON());
    }

        @Test
    public void testCreateJSONArray() throws Exception {
        System.out.println("testCreateJSONArray");
        JSON.Object<JSON.String, Value> object = new JSON.Object<JSON.String, Value>();
        object.put(new JSON.String("attribute1"), new JSON.Numeric(33));
        object.put(new JSON.String("attribute2"), new JSON.String("value"));
        JSON.Array<Value> array = new JSON.Array<Value>();
        array.add(new JSON.Numeric(10));
        array.add(new JSON.String("value"));
        array.add(JSON.TRUE);
        array.add(JSON.FALSE);
        array.add(JSON.NULL);
        array.add(object);
        JSON jsonInstance = new JSON(array);
        System.out.println(jsonInstance.toJSON());
    }
}
