/*
 *  Copyright 2011 jmarsden.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */

package jsonij.legacy;


import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.parser.ParserException;
import static org.junit.Assert.*;

public class SJTest {

    public SJTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSupportEmptyObject() throws ParserException, IOException {
        System.out.println("Support Empty Object");
        String testInput = "{}";
        String testSoultion = "{}";
        JSON json = JSON.parse(testInput);
        String testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testOutput, testSoultion);
        testInput = "{   \t\t   }";
        json = JSON.parse(testInput);
        testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testOutput, testSoultion);
    }

    @Test
    public void testSupportSimpleObjectFloatValue() throws ParserException, IOException {
        System.out.println("Support simple Object float value");
        String testInput = "{ \"PI\":3.141E-10}";
        String testSoultion = "{\"PI\":3.141E-10}";
        JSON json = JSON.parse(testInput);
        String testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
        testInput = "{  \t     \"PI\"   \t   :\t   \t3.141E-10       }";
        json = JSON.parse(testInput);
        testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
    }

    @Test
    public void testSupportLowercaseFloatValue() throws ParserException, IOException {
        System.out.println("Support lowcase float value");
        String testInput = "{ \"PI\":3.141e-10}";
        String testSoultion = "{\"PI\":3.141E-10}";
        JSON json = JSON.parse(testInput);
        String testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
        testInput = "{  \t     \"PI\" :\t   \t3.141e-10    }";
        json = JSON.parse(testInput);
        testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
    }

    @Test
    public void testSupportBigintNumberSupport() throws ParserException, IOException {
        System.out.println("Bigint number support");
        String testInput = "{ \"v\":123456789123456789123456789}";
        String testSoultion = "{\"v\":123456789123456789123456789}";
        JSON json = JSON.parse(testInput);
        String testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
        testInput = "{ \t\t  \"v\"  :   123456789123456789123456789    \t }";
        json = JSON.parse(testInput);
        testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
    }

    @Test
    public void testSupportArrayOfEmptyObject() throws ParserException, IOException {
        System.out.println("Array of empty Object");
        String testInput = "[ { }, { },[]]";
        String testSoultion = "[{},{},[]]";
        JSON json = JSON.parse(testInput);
        String testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
        testInput = "[   \t { }  \t, { }\t,[  ]]";
        json = JSON.parse(testInput);
        testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
    }

    @Test
    public void testSupportDoublePrecisionPostiveFloatingPoint() throws ParserException, IOException {
        System.out.println("Double precision postive floating point");
        String testInput = "{ \"v\":1.7976931348623157E308}";
        String testSoultion = "{\"v\":1.7976931348623157E+308}";
        JSON json = JSON.parse(testInput);
        String testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
        testInput = "{   \t \"v\"  \t :  1.7976931348623157E308  }";
        json = JSON.parse(testInput);
        testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
    }
    
    
    @Test
    public void testSupportDoublePrecisionNegativeFloatingPoint() throws ParserException, IOException {
        System.out.println("Double precision negative floating point");
        String testInput = "{ \"v\":-9.87654321921234567E-200}";
        String testSoultion = "{\"v\":-9.87654321921234567E-200}";
        JSON json = JSON.parse(testInput);
        String testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
        testInput = "{   \t \"v\"  \t :  -9.87654321921234567E-200  }";
        json = JSON.parse(testInput);
        testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
    }

    @Test
    public void testUnicode() throws ParserException, IOException {
        System.out.println("Unicode");
        String testInput = "{\"lastname\":\"\\u44ff\",\"date\":\"\\u44aa\\u44ff\",\"len\":\"\\u44AA\"}";
        String testSoultion = "{\"lastname\":\"\\u44ff\",\"date\":\"\\u44aa\\u44ff\",\"len\":\"\\u44aa\"}";
        JSON json = JSON.parse(testInput);
        String testOutput = json.toJSON();
        System.out.println(String.format("\tInput: %s\r\n\tOutput: %s", testInput, testOutput));
        assertEquals(testSoultion, testOutput);
    }


}
