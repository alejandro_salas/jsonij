/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package jsonij.legacy;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cc.plural.jsonij.JPath;
import cc.plural.jsonij.jpath.Component;
import cc.plural.jsonij.jpath.JPathParser;
import cc.plural.jsonij.jpath.KeyComponent;
import cc.plural.jsonij.jpath.LastIndexPredicate;
import cc.plural.jsonij.jpath.PredicateComponent;
import cc.plural.jsonij.jpath.SimpleIndexPredicate;
import cc.plural.jsonij.jpath.UnionPredicate;
import static org.junit.Assert.*;

/**
 *
 * @author jmarsden
 */
public class JPathParserTest {

    public JPathParserTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of parse method, of class JPathParser.
     */
    @Test
    public void testParse() throws Exception {
        System.out.println("parse");
        String testString = "/rah/boop[1]/boop[55]";
        JPathParser instance = new JPathParser();
        JPath<Component> result = instance.parse(testString);
        KeyComponent expResult1 = new KeyComponent("rah");
       KeyComponent expResult2 = new KeyComponent("boop");
        SimpleIndexPredicate expResult3 = new SimpleIndexPredicate(1);
        SimpleIndexPredicate expResult5 = new SimpleIndexPredicate(55);
        assertEquals(result.size(), 5, 0);
        assertEquals(expResult1, result.get(0));
        assertEquals(expResult2, result.get(1));
        assertEquals(expResult3, result.get(2));
        assertEquals(expResult2, result.get(3));
        assertEquals(expResult5, result.get(4));
    }

    /**
     * Test of readKey method, of class JPathParser.
     */
    @Test
    public void testReadKey() throws Exception {
        System.out.println("readKey");
        String testString = "rah/boop[1]";
        JPathParser.JPathReader target = new JPathParser.JPathReader(testString);
        JPathParser instance = new JPathParser();
        KeyComponent expResult = new KeyComponent("rah");
        KeyComponent result = instance.readKey(target);
        assertEquals(expResult, result);
        testString = "boop[1]";
 
        target = new JPathParser.JPathReader(testString);
        instance = new JPathParser();
        expResult = new KeyComponent("boop");
        result = instance.readKey(target);
        assertEquals(expResult, result);
    }

    @Test
    public void testReadSimpleIndex() throws Exception {
        System.out.println("readSimpleIndex");
        String testString = "[1]/";
        JPathParser.JPathReader target = new JPathParser.JPathReader(testString);
        JPathParser instance = new JPathParser();
        SimpleIndexPredicate expResult = new SimpleIndexPredicate(1);
        PredicateComponent result = instance.readPredicate(target);
        assertEquals(expResult, result);
        testString = "[-3]";
        target = new JPathParser.JPathReader(testString);
        expResult = new SimpleIndexPredicate(-3);
        result = instance.readPredicate(target);
        assertEquals(expResult, result);
    }

    @Test
    public void testReadLastIndex() throws Exception {
        System.out.println("readLastIndex");
        String testString = "[$]/";
        JPathParser.JPathReader target = new JPathParser.JPathReader(testString);
        JPathParser instance = new JPathParser();

        LastIndexPredicate expResult = new LastIndexPredicate();
        PredicateComponent result = instance.readPredicate(target);
        assertEquals(expResult, result);
        testString = "[last()]/";
        target = new JPathParser.JPathReader(testString);
        result = instance.readPredicate(target);
        assertEquals(expResult, result);
        testString = "[$-1]/";
        target = new JPathParser.JPathReader(testString);
        result = instance.readPredicate(target);
        expResult = new LastIndexPredicate(1);
        assertEquals(expResult, result);
        testString = "[last()-5]/";
        target = new JPathParser.JPathReader(testString);
        result = instance.readPredicate(target);
        expResult = new LastIndexPredicate(5);
        assertEquals(expResult, result);
    }

    @Test
    public void testReadAttributeIndex() throws Exception {
        System.out.println("readAttributeIndex");
        String testString = "[?(  \t\t @.firstName     <=     \"RAH\"    )  ]";
        JPathParser.JPathReader target = new JPathParser.JPathReader(testString);
        JPathParser instance = new JPathParser();
        PredicateComponent result = instance.readPredicate(target);
        System.out.println(result);
    }

        @Test
    public void testReadUnionIndex() throws Exception {
        System.out.println("readAttributeIndex");
        String testString = "[1,2,3,4,222,34,900,345,122,22,34,44,55,555,321,22,2]";
        JPathParser.JPathReader target = new JPathParser.JPathReader(testString);
        JPathParser instance = new JPathParser();
        PredicateComponent result = instance.readPredicate(target);
        PredicateComponent expResult = new UnionPredicate(new int[] {1,2,3,4,222,34,900,345,122,22,44,55,555,321});
        assertEquals(expResult, result);
    }

        
    @Test
    public void testFunctionPredicate() throws Exception {
        System.out.println("testFunctionPredicate");
        String testString = "[?(  regex(   @.firstName   ,   [    \"RAH\",   null]  ))]";
        JPathParser.JPathReader target = new JPathParser.JPathReader(testString);
        JPathParser instance = new JPathParser();
        PredicateComponent result = instance.readPredicate(target);
        System.out.println(result);
    }

        @Test
    public void testFunctionPredicateCombined() throws Exception {
        System.out.println("testFunctionPredicateCombined");
        String testString = "[?(regex(@.firstName,\"RAH\") & @.lastName=\"HAR\")]";
        JPathParser.JPathReader target = new JPathParser.JPathReader(testString);
        JPathParser instance = new JPathParser();
        PredicateComponent result = instance.readPredicate(target);
        System.out.println(result);
    }
        
}
