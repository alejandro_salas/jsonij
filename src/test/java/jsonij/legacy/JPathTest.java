/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package jsonij.legacy;

import java.io.IOException;
import java.util.Locale;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cc.plural.jsonij.JPath;
import cc.plural.jsonij.JSON;
import cc.plural.jsonij.ThreadSafeJSONParser;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.jpath.JPathParser;
import cc.plural.jsonij.jpath.JPathParserException;
import cc.plural.jsonij.parser.ParserException;
import static org.junit.Assert.*;

/**
 *
 * @author jmarsden
 */
public class JPathTest {

    ThreadSafeJSONParser parser;

    public JPathTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        parser = new ThreadSafeJSONParser();
        parser.setLocale(Locale.ENGLISH);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetMethod() throws IOException, ParserException {
        StringBuilder randomDoubleArray = new StringBuilder();
        randomDoubleArray.append("[" + Math.random());
        for (int i = 0; i < 10000; i++) {
            randomDoubleArray.append(",\t" + i);
        }
        randomDoubleArray.append("]");

        String jsonString =
                "{ \"store\": {\r\n"
                + "    \"book\": [ \r\n"
                + "      { \"category\": \"reference\",\r\n"
                + "        \"author\": \"Nigel Rees\",\r\n"
                + "        \"title\": \"Sayings of the Century\",\r\n"
                + "        \"price\": 8.95\r\n"
                + "      },\r\n"
                + "      { \"category\": \"fiction\",\r\n"
                + "        \"author\": \"Evelyn Waugh\",\r\n"
                + "        \"title\": \"Sword of Honour\",\r\n"
                + "        \"price\": 12.99\r\n"
                + "      },\r\n"
                + "      { \"category\": \"fiction\",\r\n"
                + "        \"author\": \"Herman Melville\",\r\n"
                + "        \"title\": \"Moby Dick\",\r\n"
                + "        \"isbn\": \"0-553-21311-3\",\r\n"
                + "        \"price\": 8.99,\r\n"
                + "        \"sam\": \"Awesome\"\r\n"
                + "      },\r\n"
                + "      { \"category\": \"fiction\",\r\n"
                + "        \"author\": \"J. R. R. Tolkien\",\r\n"
                + "        \"title\": \"The Lord of the Rings\",\r\n"
                + "        \"isbn\": \"0-395-19395-8\",\r\n"
                + "        \"price\": 22.99\r\n"
                + "      }\r\n"
                + "    ],\r\n"
                + "    \"bicycle\": {\r\n"
                + "      \"color\": \"red\",\r\n"
                + "      \"price\": 19.95\r\n"
                + "    },\r\n"
                + "    \"number\": " + randomDoubleArray.toString() + ""
                + "  }\r\n"
                + "}";
        JSON json = JSON.parse(jsonString);
        System.out.println(json.getRoot().getValueType() + "(" + json.getRoot().nestedSize() + "): " + json.getRoot().toJSON());

        JPathParser jPathParser = new JPathParser();
        JPath<?> path1 = jPathParser.parse("/store");
        path1.setRecordEvaluateTime(true);
        Value value = path1.evaluate(json);
        System.out.println(value.getValueType() + "(" + value.nestedSize() + "): " + value.toJSON() + " (in " + path1.getLastEvaluateTime() + " ms)");

        JPath<?> path2 = jPathParser.parse("/store/book[-2]");
        path2.setRecordEvaluateTime(true);
        value = path2.evaluate(json);
        System.out.println(value.getValueType() + "--> (" + value.nestedSize() + "): " + value.toJSON() + " (in " + path2.getLastEvaluateTime() + " ms)");

        JPath<?> path3 = jPathParser.parse("/store/book[2]/title");
        path3.setRecordEvaluateTime(true);
        value = path3.evaluate(json);
        System.out.println(value.getValueType() + "(" + value.nestedSize() + "): " + value.toJSON() + " (in " + path3.getLastEvaluateTime() + " ms)");

        JPath<?> path4 = jPathParser.parse("/store/book[$]/title");
        path4.setRecordEvaluateTime(true);
        value = path4.evaluate(json);
        System.out.println(value.getValueType() + "(" + value.nestedSize() + "): " + value.toJSON() + " (in " + path4.getLastEvaluateTime() + " ms)");

        JPath<?> path5 = jPathParser.parse("/store/book[last()-3]/title");
        path5.setRecordEvaluateTime(true);
        value = path5.evaluate(json);
        System.out.println(value.getValueType() + "(" + value.nestedSize() + "): " + value.toJSON() + " (in " + path5.getLastEvaluateTime() + " ms)");

        JPath<?> path6 = jPathParser.parse("/store/book[last()-1337]/title");
        try {
            value = path6.evaluate(json);
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }

        JPath<?> path7 = JPath.parse("/store/number[$-1337]");
        path7.setRecordEvaluateTime(true);
        value = path7.evaluate(json);
        System.out.println(value.getValueType() + "(" + value.nestedSize() + "): " + value.toJSON() + " (in " + path7.getLastEvaluateTime() + " ms)");

    }

    @Test
    public void testExpression0() throws IOException, ParserException {
        System.out.println("Test Expression0 - Single Expression Query - String and Double equals");
        JSON json = JSON.parse(JSONTestExamples.STORE_SAMPLE);

        JPathParser jPathParser = new JPathParser();
        JPath<?> path1 = jPathParser.parse("/store");
        path1.setRecordEvaluateTime(true);
        Value value1 = path1.evaluate(json);
        System.out.println(value1.getValueType() + "(" + value1.nestedSize() + "): " + value1.toJSON() + " (in " + path1.getLastEvaluateTime() + " ms)");

        JPath<?> path2 = jPathParser.parse("/store/book[?(@.title=\"Sword of Honour\")]/author");
        path2.setRecordEvaluateTime(true);
        Value value2 = path2.evaluate(json);
        System.out.println(value2.getValueType() + "(" + value2.nestedSize() + "): " + value2.toJSON() + " (in " + path2.getLastEvaluateTime() + " ms)");

        JPath<?> path3 = jPathParser.parse("/book[?(@.author=\"J. R. R. Tolkien\")]/title");
        path3.setRecordEvaluateTime(true);
        Value value3 = path3.evaluate(value1);
        System.out.println(value3.getValueType() + "(" + value3.nestedSize() + "): " + value3.toJSON() + " (in " + path3.getLastEvaluateTime() + " ms)");

        JPath<?> path4 = jPathParser.parse("/store/book[?(@.price=8.99)]/title");
        path4.setRecordEvaluateTime(true);
        Value value4 = path4.evaluate(json);
        System.out.println(value4.getValueType() + "(" + value4.nestedSize() + "): " + value4.toJSON() + " (in " + path4.getLastEvaluateTime() + " ms)");
    }

    @Test
    public void testExpression1() throws IOException, ParserException {
        System.out.println("Test Expression1 - Single Expression Query String");
        JSON json = JSON.parse(JSONTestExamples.RESUME_SAMPLE);

        JPathParser jPathParser = new JPathParser();
        JPath<?> path1 = jPathParser.parse("/resume/misc\\/other[0]");
        path1.setRecordEvaluateTime(true);
        Value value1 = path1.evaluate(json);
        System.out.println(value1.getValueType() + "(" + value1.nestedSize() + "): " + value1.toJSON() + " (in " + path1.getLastEvaluateTime() + " ms)");

        assertEquals("Matrikon Asia-Pacific Heart Award 2005.", value1.getString());

        JPath<?> path2 = jPathParser.parse("/resume/employment[?(@.city=\"Sydney\")]/employer");
        path2.setRecordEvaluateTime(true);
        Value value2 = path2.evaluate(json);
        System.out.println(value1.getValueType() + "(" + value2.nestedSize() + "): " + value2.toJSON() + " (in " + path2.getLastEvaluateTime() + " ms)");

        assertEquals("IBM", value2.getString());
    }

    @Test
    public void testExpression2() throws IOException, ParserException {
        System.out.println("Test Expression2 - Multiple and Expression Query");

        JSON json = JSON.parse(JSONTestExamples.RESUME_SAMPLE);

        JPathParser jPathParser = new JPathParser();

        JPath<?> path1 = jPathParser.parse("/resume/employment[?(@.city=\"Newcastle\" && @.start date={\"day\":1,\"month\":8,\"year\":2003})]/employer");
        path1.setRecordEvaluateTime(true);
        Value value1 = path1.evaluate(json);
        System.out.println(value1.getValueType() + "(" + value1.nestedSize() + "): " + value1.toJSON() + " (in " + path1.getLastEvaluateTime() + " ms)");

        assertEquals("Matrikon", value1.getString());
    }

    @Test
    public void testExpression3() throws IOException, ParserException {
        System.out.println("Test Expression3 - Memory Test Overflow");
        String startDate1 = "2010-01-01";
        String endDate1 = "2011-01-01";
        String startDate2 = "2010-01-01";
        JSON json = JSON.parse(JSONTestExamples.METER_SAMPLE);
        assertEquals(startDate1, JPath.parse("/enrollments[?(@.key=\"Carbon\")]/start").evaluate(json).getString());
        assertEquals(endDate1, JPath.parse("/enrollments[?(@.key=\"Carbon\")]/end").evaluate(json).getString());
        try {
            JPath.parse("/enrollments[?(@.key=\"Carbon\"]/channels[?(@.channelid=\"CH-0\"]/start").evaluate(json).getString();
        }
        catch (JPathParserException e) {
            System.out.println("Error missing ')'" + e.toString());
        }
    }

    @Test
    public void testExpression4() throws IOException, ParserException {
        System.out.println("Test Expression4 - Working Query");
        String startDate1 = "2010-01-01";
        String endDate1 = "2011-01-01";
        String startDate2 = "2010-01-01";
        JSON json = JSON.parse(JSONTestExamples.METER_SAMPLE);
        assertEquals(startDate1, JPath.parse("/enrollments[?(@.key=\"Carbon\")]/start").evaluate(json).getString());
        assertEquals(endDate1, JPath.parse("/enrollments[?(@.key=\"Carbon\")]/end").evaluate(json).getString());
        assertEquals(startDate2, JPath.parse("/enrollments[?(@.key=\"Carbon\")]/channels[?(@.channelid=\"CH-0\")]/start").evaluate(json).getString());
    }

    @Test
    public void testExpression5() throws IOException, ParserException {
        System.out.println("Test Expression5 - Working Query");
        JSON json = JSON.parse(JSONTestExamples.MC_EXAMPLE);
        Value settingsValue = JPath.parse("/settings").evaluate(json);
        System.out.println("Settings:" + settingsValue);
        assertEquals("java", JPath.parse("/settings/java exe").evaluate(json).getString());
    }

    @Test
    public void testExpression6() throws IOException, ParserException {
        System.out.println("Test testExpression6 - Union Query");
        JSON json = JSON.parse(JSONTestExamples.STORE_SAMPLE);
        Value[] values = JPath.parse("/store/book[2,3,0,-1]").evaluateAll(json);
        System.out.println("ResultSize:" + values.length);
        for (int i = 0; i < values.length; i++) {
            System.out.println("[" + i + "]" + values[i].toJSON());
        }
    }

    @Test
    public void testExpression7() throws Exception {
        System.out.println("testExpression7");
        JSON json = JSON.parse(JSONTestExamples.STORE_SAMPLE);
        Value[] values = JPath.parse("/store/bicycle[?(regex(@.color,\"^re.*$\"))]").evaluateAll(json);
        System.out.println("ResultSize:" + values.length);
        for (int i = 0; i < values.length; i++) {
            System.out.println("[" + i + "]" + values[i].toJSON());
        }
    }

    @Test
    public void testExpression8() throws Exception {
        System.out.println("testExpression8");
        JSON json = JSON.parse(JSONTestExamples.STORE_SAMPLE);
        Value[] values = JPath.parse("/store/book[?(regex(@.title,\"^.*Lord.*$\"))]").evaluateAll(json);
        System.out.println("ResultSize:" + values.length);
        for (int i = 0; i < values.length; i++) {
            System.out.println("[" + i + "]" + values[i].toJSON());
        }
    }

    @Test
    public void testExpression9() throws Exception {
        System.out.println("testExpression9");
        JSON json = JSON.parse(JSONTestExamples.STORE_SAMPLE);
        Value[] values = JPath.parse("/store/book[?(regex(@.title,\"^.*of.*$\"))]").evaluateAll(json);
        System.out.println("ResultSize:" + values.length);
        for (int i = 0; i < values.length; i++) {
            System.out.println("[" + i + "]" + values[i].toJSON());
        }
    }

    @Test
    public void testExpression10() throws Exception {
        System.out.println("testExpression10");
        JSON json = JSON.parse(JSONTestExamples.STORE_SAMPLE);
        Value[] values = JPath.parse("/store/bicycle[?(regex(@.color,\"^blue.*$\"))]").evaluateAll(json);
        System.out.println("ResultSize:" + values.length);
        for (int i = 0; i < values.length; i++) {
            System.out.println("[" + i + "]" + values[i].toJSON());
        }
    }

    @Test
    public void testExpression11() throws IOException, ParserException {
        System.out.println("Test Expression11 - Array Start Query");
        JSON json = JSON.parse("[{\"blah\":1},{\"blah\":2},{\"blah\":3},{\"blah\":4}]");
        Value value = JPath.parse("/[2]/blah").evaluate(json);
        System.out.println("Value:" + value);
        assertEquals(value.toString(), "3");
    }

    @Test
    public void testExpression12() throws IOException, ParserException {
        System.out.println("Test Expression12 - Array Start Query2");
        JSON json = JSON.parse("   [\r\n"
                + "     {\r\n"
                + "       \"category\": \"reference\",\r\n"
                + "         \"author\": \"Nigel Rees\",\r\n"
                + "          \"title\": \"Sayings of the Century\",\r\n"
                + "          \"price\": 8.95\r\n"
                + "     }\r\n"
                + "   ]");
        Value value = JPath.parse("/[0]/[?(@.author=\"Nigel Rees\")]/author").evaluate(json);
        System.out.println("Value:" + value);
        assertEquals(value.toString(), "Nigel Rees");
    }
}
