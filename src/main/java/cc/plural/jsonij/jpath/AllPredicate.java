/*
 *  Copyright 2011 jmarsden.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package cc.plural.jsonij.jpath;

import java.util.List;

import cc.plural.jsonij.Value;

import static cc.plural.jsonij.Value.TYPE.ARRAY;
import static cc.plural.jsonij.Value.TYPE.OBJECT;

/**
 * All predicate implementation.
 *
 * @author J.W.Marsden
 */
public class AllPredicate extends PredicateComponent {

    /**
     * If the value is an array, then add all elements. If the
     * value is an object, then add all attributes.
     * 
     * @param values The current values.
     * @return The new values.
     */
    @Override
    public List<Value> evaluate(List<Value> values, List<Value> results) {
        for (Value value : values) {
            if(value.type() == ARRAY) {
                for(int i=0;i<value.size();i++) {
                    results.add(value.get(i));
                }
            } else if(value.type() == OBJECT) {
                for(int i=0;i<value.size();i++) {
                    results.add(value.get(i));
                }
            }
        }
        return results;
    }
}
