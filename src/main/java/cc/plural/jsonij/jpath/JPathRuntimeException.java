/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package cc.plural.jsonij.jpath;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Internationised Parser Exception.
 *
 * @author openecho
 * @version 1.0.0
 */
public class JPathRuntimeException extends RuntimeException {

    /**
	 * Serial UID
	 */
	private static final long serialVersionUID = -4761587256432880263L;

	public static final String MESSAGE_BUNDLE = "JPathMessageBundle";

    /**
     * Exception Key
     */
    protected String key;

    protected String message;
    /**
     * Exception Locale
     */
    protected Locale locale;

    public JPathRuntimeException(String key, Object... args) {
        this(key, null, args);
    }

    public JPathRuntimeException(String key, Locale locale, Object... args) {
        this.key = key;
        this.locale = ((locale == null) ? Locale.ENGLISH : locale);
        if (this.locale != null) {
            String messageFormat = ResourceBundle.getBundle(getBundleName(), this.locale).getString(this.key);
            this.message = String.format(messageFormat, args);
        } else {
            this.message = String.format("Undefined Exception %s %s", key, locale);
        }
    }

    public String getBundleName() {
        return MESSAGE_BUNDLE;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
