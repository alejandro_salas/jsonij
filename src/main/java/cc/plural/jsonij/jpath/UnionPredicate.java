/*
 *  Copyright 2011 jmarsden.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package cc.plural.jsonij.jpath;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import cc.plural.jsonij.Value;

/**
 * JPath Union Predicate Implementation.
 *
 * This predicate allows a comma separated zero based list of indexes for JPath
 * evaluation. For example, [0,2] will result in the first and third indexes
 * of an array being evaluated in the JPath.
 *
 * Indexes must be <code>0 <= x <= R</code>.
 *
 * @author J.W.Marsden
 */
public class UnionPredicate extends PredicateComponent {

    public final int CAPACITY_GROWTH_FACTOR = 10;
    int capacity;
    int current;
    int[] indexes;

    public UnionPredicate() {
        capacity = 10;
        current = 0;
        this.indexes = new int[capacity];
    }

    public UnionPredicate(int[] indexes) {
        this.capacity = Array.getLength(indexes);
        this.current = capacity;
        this.indexes = indexes;
    }

    public int[] getIndexes() {
        if (capacity == current) {
            return indexes;
        } else {
            int[] result = new int[current];
            System.arraycopy(indexes, 0, result, 0, current);
            return result;
        }
    }

    public void setIndexes(int[] indexes) {
        this.capacity = Array.getLength(indexes);
        this.current = capacity;
        this.indexes = indexes;
    }

    public boolean containsIndex(int index) {
        boolean result = false;
        for (int i = 0; i < current; i++) {
            if (indexes[i] == index) {
                result = true;
                break;
            }
        }
        return result;
    }

    public void addIndex(int index) {
        if (!containsIndex(index)) {
            if (current < capacity) {
                indexes[current] = index;
                current++;
            } else {
                int[] newIndexes = new int[capacity + CAPACITY_GROWTH_FACTOR];
                System.arraycopy(indexes, 0, newIndexes, 0, current);
                capacity = capacity + CAPACITY_GROWTH_FACTOR;
                newIndexes[current] = index;
                indexes = newIndexes;
                current++;
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnionPredicate other = (UnionPredicate) obj;
        if (!Arrays.equals(this.getIndexes(), other.getIndexes())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Arrays.hashCode(this.getIndexes());
        return hash;
    }

    @Override
    public List<Value> evaluate(List<Value> values, List<Value> results) {
        for(Value value: values) {
            int currentValueSize = value.size();
            for(int j=0;j<Array.getLength(indexes);j++) {
                if(indexes[j] >= 0 && indexes[j] < currentValueSize) {
                    results.add(value.get(indexes[j]));
                } else if(indexes[j] < 0 && (currentValueSize - indexes[j]) >= 0) {
                    results.add(value.get(currentValueSize + indexes[j]));
                }
            }
        }
        return results;
    }
}
