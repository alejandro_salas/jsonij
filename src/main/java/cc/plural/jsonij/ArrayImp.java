/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package cc.plural.jsonij;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * JSON Array Implementation.
 * @author J.W.Marsden
 * @version 1.0.0
 */
public class ArrayImp<E extends Value> extends Value implements java.util.List<E> {

    /**
	 * Generated Serial UID
	 */
	private static final long serialVersionUID = -8216125868251081773L;
	/**
	 * Array Contents Holder
	 */
	protected List<E> arrayValue;

    public ArrayImp() {
        arrayValue = new ArrayList<E>();
    }

    /* (non-Javadoc)
     * @see com.realitypipe.json.Value#internalType()
     */
    @Override
    protected TYPE internalType() {
        return TYPE.ARRAY;
    }

    /* (non-Javadoc)
     * @see java.util.List#add(java.lang.Object)
     */
    public boolean add(E e) {
        return arrayValue.add(e);
    }

    /* (non-Javadoc)
     * @see java.util.List#add(int, java.lang.Object)
     */
    public void add(int index, E element) {
        arrayValue.add(index, element);
    }

    /* (non-Javadoc)
     * @see java.util.List#addAll(java.util.Collection)
     */
    public boolean addAll(Collection<? extends E> c) {
        return arrayValue.addAll(c);
    }

    /* (non-Javadoc)
     * @see java.util.List#addAll(int, java.util.Collection)
     */
    public boolean addAll(int index, Collection<? extends E> c) {
        return arrayValue.addAll(index, c);
    }

    /* (non-Javadoc)
     * @see java.util.List#clear()
     */
    public void clear() {
        arrayValue.clear();
    }

    /* (non-Javadoc)
     * @see java.util.List#contains(java.lang.Object)
     */
    public boolean contains(Object o) {
        return arrayValue.contains(o);
    }

    /* (non-Javadoc)
     * @see java.util.List#containsAll(java.util.Collection)
     */
    public boolean containsAll(Collection<?> c) {
        return arrayValue.containsAll(c);
    }

    /* (non-Javadoc)
     * @see java.util.List#get(int)
     */
    public E get(int index) {
        return arrayValue.get(index);
    }

    /* (non-Javadoc)
     * @see java.util.List#indexOf(java.lang.Object)
     */
    public int indexOf(Object o) {
        return arrayValue.indexOf(o);
    }

    /* (non-Javadoc)
     * @see java.util.List#isEmpty()
     */
    public boolean isEmpty() {
        return arrayValue.isEmpty();
    }

    /* (non-Javadoc)
     * @see java.util.List#iterator()
     */
    public Iterator<E> iterator() {
        return arrayValue.iterator();
    }

    /* (non-Javadoc)
     * @see java.util.List#lastIndexOf(java.lang.Object)
     */
    public int lastIndexOf(Object o) {
        return arrayValue.lastIndexOf(o);
    }

    /* (non-Javadoc)
     * @see java.util.List#listIterator()
     */
    public ListIterator<E> listIterator() {
        return arrayValue.listIterator();
    }

    /* (non-Javadoc)
     * @see java.util.List#listIterator(int)
     */
    public ListIterator<E> listIterator(int index) {
        return arrayValue.listIterator();
    }

    /* (non-Javadoc)
     * @see java.util.List#remove(java.lang.Object)
     */
    public boolean remove(Object o) {
        return arrayValue.remove(o);
    }

    /* (non-Javadoc)
     * @see java.util.List#remove(int)
     */
    public E remove(int index) {
        return arrayValue.get(index);
    }

    /* (non-Javadoc)
     * @see java.util.List#removeAll(java.util.Collection)
     */
    public boolean removeAll(Collection<?> c) {
        return arrayValue.removeAll(c);
    }

    /* (non-Javadoc)
     * @see java.util.List#retainAll(java.util.Collection)
     */
    public boolean retainAll(Collection<?> c) {
        return arrayValue.retainAll(c);
    }

    /* (non-Javadoc)
     * @see java.util.List#set(int, java.lang.Object)
     */
    public E set(int index, E element) {
        return arrayValue.set(index, element);
    }

    /* (non-Javadoc)
     * @see java.util.List#size()
     */
    public int size() {
        return arrayValue.size();
    }

    /* (non-Javadoc)
     * @see java.util.List#subList(int, int)
     */
    public List<E> subList(int fromIndex, int toIndex) {
        return arrayValue.subList(fromIndex, toIndex);
    }

    /* (non-Javadoc)
     * @see java.util.List#toArray()
     */
    public Object[] toArray() {
        return arrayValue.toArray();
    }

    /* (non-Javadoc)
     * @see java.util.List#toArray(T[])
     */
    public <T> T[] toArray(T[] a) {
        return arrayValue.toArray(a);
    }

    /* (non-Javadoc)
     * @see cc.plural.json.Value#nestedSize()
     */
    @Override
    public int nestedSize() {
        int c = 0;
        for (E e : this) {
            c += e.nestedSize();
        }
        return size() + c;
    }

    /* (non-Javadoc)
     * @see cc.plural.json.Value#toJSON()
     */
    @Override
    public String toJSON() {
        Iterator<E> valueIterator = iterator();
        if(valueIterator.hasNext()) {
            StringBuilder jsonStringBuilder = new StringBuilder();
            jsonStringBuilder.append('[');
            Value value = valueIterator.next();
            jsonStringBuilder.append(value.toJSON());
            while(valueIterator.hasNext()) {
                value = valueIterator.next();
                jsonStringBuilder.append(',').append(value.toJSON());
            }
            jsonStringBuilder.append(']');
            return jsonStringBuilder.toString();
        } else {
            return "[]";
        }
    }
}
