/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package cc.plural.jsonij.parser;

import java.io.IOException;

import cc.plural.jsonij.parser.ParserException;
import cc.plural.jsonij.parser.Position;

/**
 *
 * @author openecho
 */
public interface ReaderParser {

    /**
     * @return the position
     */
    public Position getPosition();

    /**
     * Only here for POJO reasons.
     * @return hasPeeked()
     * @see hasPeeked()
     */
    public boolean isHasPeeked();

    public boolean hasPeeked();

    public void setHasPeeked(boolean hasPeeked);

    public int getLineNumber();

    public int getPositionNumber();

    public int peek() throws IOException, ParserException;

    public int read() throws IOException, ParserException;

    public void close();
}
