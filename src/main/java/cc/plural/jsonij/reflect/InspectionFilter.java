/*
 *  Copyright 2011 jmarsden.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package cc.plural.jsonij.reflect;

import java.util.ArrayList;
import java.util.List;

public class InspectionFilter {

    protected List<Class<?>> classFilter;

    protected static InspectionFilter defaultFilters;

    static {
        defaultFilters = null;
    }

    public InspectionFilter() {
        classFilter = new ArrayList<Class<?>>();
    }

    public boolean isFiltered(Class<?> clazz) {
        return classFilter.contains(clazz);
    }

    protected static void createDefaultInstance() {
        defaultFilters = new InspectionFilter();
        defaultFilters.classFilter.add(java.lang.Class.class);
        defaultFilters.classFilter.add(java.lang.Object.class);
        defaultFilters.classFilter.add(java.util.List.class);
        defaultFilters.classFilter.add(java.util.ArrayList.class);
        defaultFilters.classFilter.add(java.util.Map.class);
        defaultFilters.classFilter.add(java.util.HashMap.class);
    }

    public static InspectionFilter getDefaultFilters() {
        if(defaultFilters == null) {
            createDefaultInstance();
        }
        return defaultFilters;
    }
}
