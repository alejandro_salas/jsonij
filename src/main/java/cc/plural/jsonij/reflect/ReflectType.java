/*
 * Copyright 2011 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.reflect;

import cc.plural.jsonij.Value;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cc.plural.jsonij.marshal.JSONMarshaler;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 * JavaType Utility. Queries objects for their types and manages inspector
 * instances so time is not wasted re-inspecting the same objects.
 *
 * Inspecting is the task of reflecting attributes and methods and their types
 * for access.
 *
 * @author jmarsden@plural.cc
 */
public enum ReflectType {

    /**
     * The JavaTypes possible for an inspection. UNKNOWN will be returned where
     * an object cannot be decided. ARRAY will be returned for an array of
     * Objects.
     */
    BOOLEAN,
    BYTE,
    SHORT,
    INTEGER,
    FLOAT,
    DOUBLE,
    LONG,
    STRING,
    LIST,
    OBJECT,
    MAP,
    ENUM,
    ARRAY,
    ARRAY_BOOLEAN,
    ARRAY_BYTE,
    ARRAY_SHORT,
    ARRAY_INTEGER,
    ARRAY_FLOAT,
    ARRAY_DOUBLE,
    ARRAY_LONG,
    ARRAY_STRING,
    ARRAY_ENUM,
    // HMMN
    ARRAY_LIST,
    ARRAY_MAP,
    ARRAY_ARRAY,
    CLASS,
    JSON_VALUE,
    UNKOWN;
    boolean primitive;
    JavaArrayType arrayType;
    static final protected InspectionFactory inspectionFactory;
    static final protected Map<Class<?>, Inspection> inspectedClasses;

    static {
        inspectionFactory = new InspectionFactory();
        inspectedClasses = new HashMap<Class<?>, Inspection>();
    }

    ReflectType() {
        primitive = true;
    }

    public void setPrimitive(boolean primitive) {
        this.primitive = primitive;
    }

    public boolean isPrimitive() {
        return this.primitive;
    }

    public static ReflectType inspectObjectType(Class<?> c) {
        ReflectType type = null;
        if (c == boolean.class) {
            type = BOOLEAN;
            type.setPrimitive(true);
            return type;
        } else if (c == Boolean.class) {
            type = BOOLEAN;
            type.setPrimitive(false);
            return type;
        } else if (c == int.class) {
            type = INTEGER;
            type.setPrimitive(true);
            return type;
        } else if (c == Integer.class) {
            type = INTEGER;
            type.setPrimitive(false);
            return type;
        } else if (c == BigInteger.class) {
            type = INTEGER;
            type.setPrimitive(false);
            return type;
        } else if (c == double.class) {
            type = DOUBLE;
            type.setPrimitive(true);
            return type;
        } else if (c == Double.class) {
            type = DOUBLE;
            type.setPrimitive(false);
            return type;
        } else if (c == BigDecimal.class) {
            type = DOUBLE;
            type.setPrimitive(false);
            return type;
        } else if (c == float.class) {
            type = FLOAT;
            type.setPrimitive(true);
            return type;
        } else if (c == Float.class) {
            type = FLOAT;
            type.setPrimitive(false);
            return type;
        } else if (c == long.class) {
            type = LONG;
            type.setPrimitive(true);
            return type;
        } else if (c == Long.class) {
            type = LONG;
            type.setPrimitive(false);
            return type;
        } else if (c == short.class) {
            type = SHORT;
            type.setPrimitive(true);
            return type;
        } else if (c == Short.class) {
            type = SHORT;
            type.setPrimitive(false);
            return type;
        } else if (c == byte.class) {
            type = BYTE;
            type.setPrimitive(true);
            return type;
        } else if (c == Byte.class) {
            type = BYTE;
            type.setPrimitive(false);
            return type;
        } else if (c.isEnum()) {
            type = ENUM;
            type.setPrimitive(false);
            return type;
        } else if (c == String.class || c == CharSequence.class || ((c == char.class || c == Character.class))) {
            type = STRING;
            type.setPrimitive(false);
            return type;
        } else if (c.isArray()) {
            type = getArrayType(c);
            return type;
        }

        if(c == Class.class) {
            return CLASS;
        }

        if (c == List.class) {
            return LIST;
        }
        if (c == Map.class) {
            return MAP;
        }

        Class<?>[] interfaces;
        Class<?> currentClass = c;

        do {
            if(currentClass == Value.class) {
                type = JSON_VALUE;
                break;
            }
            interfaces = currentClass.getInterfaces();
            for (int i = 0; i < Array.getLength(interfaces); i++) {
                if (interfaces[i] == List.class) {
                    type = LIST;
                    break;
                } else if (interfaces[i] == Map.class) {
                    type = MAP;
                    break;
                } else if (interfaces[i] == CharSequence.class) {
                    type = STRING;
                    break;
                } else if (interfaces[i] == Value.class){
                    type = JSON_VALUE;
                    break;
                }
            }
        } while ((currentClass = currentClass.getSuperclass()) != null);

        if ((JSONMarshaler.ALWAYS_USE_INNER_PROPERTY && (type != null)) || isObjectType(c)) {
            type = OBJECT;
            type.setPrimitive(false);
            return type;
        }
        if (c == Object.class) {
            type = OBJECT;
            return type;
        }
        if (c.getComponentType() == Class.class) {
            type = OBJECT;
            return type;
        }
        if (type != null) {
            return type;
        } else {
            return UNKOWN;
        }
    }

    public static Inspection getInspection(Class<?> objectClass) {
        Inspection inspection = null;
        synchronized (inspectedClasses) {
            if (inspectedClasses.containsKey(objectClass)) {
                inspection = inspectedClasses.get(objectClass);
            } else {
                inspection = inspectionFactory.inspect(objectClass);
                inspectedClasses.put(objectClass, inspection);
            }
        }
        return inspection;
    }

    public static boolean isObjectType(Class<?> objectClass) {
        Inspection inspection = getInspection(objectClass);
        List<ClassProperty> properties = inspection.getProperties();
        if ((properties != null && !properties.isEmpty())/* || inspector.hasInnerArray() || inspector.hasInnerObject() */) {
            return true;
        }
        return false;
    }

    @SuppressWarnings("unused")
    public static ReflectType getArrayType(Class<?> objectClass) {
        ReflectType resultType = null;
        Class<?> objectComponentType = objectClass.getComponentType();

        if (!objectComponentType.isArray()) {


            ReflectType componentType = inspectObjectType(objectComponentType);
            //resultType.arrayType = new JavaArrayType(objectClass);
            switch (componentType) {
                case BOOLEAN:
                    resultType = ReflectType.ARRAY_BOOLEAN;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                case BYTE:
                    resultType = ReflectType.ARRAY_BYTE;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                case INTEGER:
                    resultType = ReflectType.ARRAY_INTEGER;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                case SHORT:
                    resultType = ReflectType.ARRAY_SHORT;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                case FLOAT:
                    resultType = ReflectType.ARRAY_FLOAT;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                case LONG:
                    resultType = ReflectType.ARRAY_LONG;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                case DOUBLE:
                    resultType = ReflectType.ARRAY_DOUBLE;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                case STRING:
                    resultType = ReflectType.ARRAY_STRING;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                case ENUM:
                    resultType = ReflectType.ARRAY_ENUM;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                case MAP:
                    resultType = ReflectType.ARRAY_MAP;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                case LIST:
                    resultType = ReflectType.ARRAY_LIST;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                case OBJECT:
                    resultType = ReflectType.ARRAY;
                    resultType.setPrimitive(componentType.isPrimitive());
                    break;
                default: 
                    System.out.println("Unknown Type!!");
                    Thread.dumpStack();
                    throw new RuntimeException("Unhandled Type " + componentType);
            }
        } else {
            resultType = ReflectType.ARRAY_ARRAY;
            resultType.setPrimitive(false);
        }

        if (resultType != null) {
            return resultType;
        } else {
            return ReflectType.UNKOWN;
        }
    }

    public static class JavaArrayType {

        Class<?> arrayType;
        int dimension;

        public JavaArrayType(Class<?> arrayType) {
            this.arrayType = arrayType;
            dimension = 0;
            this.inspect();
        }

        public final void inspect() {
            if (arrayType != null && arrayType.isArray()) {
                Class<?> innerType = arrayType.getComponentType();
                dimension = 1;
                while (innerType.isArray() && (innerType = arrayType.getComponentType()) != null) {
                    dimension++;
                }
            }
        }

        @Override
        public String toString() {
            return "Array " + arrayType.getSimpleName() + "[" + dimension + "]";
        }
    }

    public static List<Class<?>> getInterfaceTrace(Class<?> base) {
        List<Class<?>> interfaceTrace = new ArrayList<Class<?>>();
        Class[] interfaces;
        Class currentClass = base;
        do {
            interfaces = currentClass.getInterfaces();
            for (int i = 0; i < Array.getLength(interfaces); i++) {
                if (!interfaceTrace.contains(interfaces[i])) {
                    interfaceTrace.add(interfaces[i]);
                }
            }
            if (!interfaceTrace.contains(currentClass)) {
                interfaceTrace.add(currentClass);
            }
        } while ((currentClass = currentClass.getSuperclass()) != null);
        return interfaceTrace;
    }
}
