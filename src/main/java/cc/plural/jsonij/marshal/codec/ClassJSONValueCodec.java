/*
 * Copyright 2012 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.marshal.codec;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.JSONMarshalerException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jmarsden
 */
public class ClassJSONValueCodec implements JSONValueCodec {

    public static <D extends Class> Value encode(D d) {
        JSON.String encoded = new JSON.String(d.getCanonicalName());
        return encoded;
    }

    public static <D extends Class<?>> Class<?> decode(Value value, D clazz) throws JSONMarshalerException {
        if (value.getValueType() == Value.TYPE.STRING) {
            try {
                Class<?> decode = Class.forName(value.toString());
                return decode;
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ClassJSONValueCodec.class.getName()).log(Level.SEVERE, null, ex);
            }
            throw new JSONMarshalerException("decodeError");
        } else {
            throw new JSONMarshalerException("decodeError");
        }
    }
}
