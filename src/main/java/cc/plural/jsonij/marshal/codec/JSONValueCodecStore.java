/*
 * Copyright 2012 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.marshal.codec;

import cc.plural.jsonij.Value;
import cc.plural.jsonij.reflect.ReflectType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jmarsden
 */
public class JSONValueCodecStore {

    public static final String ENCODE_METHOD_SIGNATURE;
    public static final String DECODE_METHOD_SIGNATURE;
    public static Map<String, JSONValueCodecHelper> codecs;

    static {
        ENCODE_METHOD_SIGNATURE = "encode";
        DECODE_METHOD_SIGNATURE = "decode";
    }

    public static void clearCodecs() {
        if (codecs != null) {
            codecs = null;
        }
    }

    public boolean hasCodec(Class<?> type) {
        String name = type.getName();
        if (codecs == null) {
            return false;
        }
        if (codecs.containsKey(name)) {
            return true;
        }
        // Check Parents
        List<Class<?>> interfaceTrace = ReflectType.getInterfaceTrace(type);
        for (Class<?> i : interfaceTrace) {
            String interfaceName = i.getName();
            if (codecs.containsKey(interfaceName)) {
                return true;
            }
        }
        return false;
    }

    public void registerCodec(Class<?> type, Class<? extends JSONValueCodec> codec) {
        String name = type.getName();
        if (codecs != null && codecs.containsKey(name)) {
            return;
        } else if (codecs == null) {
            codecs = new HashMap<String, JSONValueCodecHelper>();
        }
        
        JSONValueCodecHelper helper = new JSONValueCodecHelper();
        helper.type = type;


//        List<Class<?>> genericTypes = new ArrayList<Class<?>>();
//        TypeVariable<Class<?>>[] parameterTypes = (TypeVariable<Class<?>>[]) type.getTypeParameters();
//        for (TypeVariable<Class<?>> parameterType : parameterTypes) {
//            Type[] bounds = parameterType.getBounds();
//            System.out.println(bounds[0]);
//        }



        helper.codec = codec;
        Method[] methods = codec.getMethods();
        for (Method method : methods) {
            if (method.getName().equals(ENCODE_METHOD_SIGNATURE)) {
                helper.encodeMethod = method;
            } else if (method.getName().equals(DECODE_METHOD_SIGNATURE)) {
                helper.decodeMethod = method;
            }
        }
        codecs.put(name, helper);
    }

    public JSONValueCodecHelper getCodecHelper(Class<?> type) {
        String name = type.getName();
        
        if (codecs.containsKey(name)) {
            return codecs.get(name);
        } else {
            // Check Parents
            List<Class<?>> interfaceTrace = ReflectType.getInterfaceTrace(type);
            for (Class<?> i : interfaceTrace) {
                String interfaceName = i.getName();
                if (codecs.containsKey(interfaceName)) {
                    return codecs.get(interfaceName);
                }
            }
        }
        return null;
    }

    public Class<? extends JSONValueCodec> getCodec(Class<?> type) {
        String name = type.getName();
        if (codecs.containsKey(name)) {
            return codecs.get(name).getCodec();
        } else {
            // Check Parents
            List<Class<?>> interfaceTrace = ReflectType.getInterfaceTrace(type);
            for (Class<?> i : interfaceTrace) {
                 String interfaceName = i.getName();
                if (codecs.containsKey(interfaceName)) {
                    return codecs.get(interfaceName).getCodec();
                }
            }
        }
        return null;
    }

    public static class JSONValueCodecHelper {

        private Class<?> type;
        //private Class<?>[] genericTypes;
        private Class<? extends JSONValueCodec> codec;
        private Method encodeMethod;
        private Method decodeMethod;

        public Class<? extends JSONValueCodec> getCodec() {
            return codec;
        }

        public void setCodec(Class<? extends JSONValueCodec> codec) {
            this.codec = codec;
        }

        public Method getEncodeMethod() {
            return encodeMethod;
        }

        public void setEncodeMethod(Method encodeMethod) {
            this.encodeMethod = encodeMethod;
        }

        public Method getDecodeMethod() {
            return decodeMethod;
        }

        public void setDecodeMethod(Method decodeMethod) {
            this.decodeMethod = decodeMethod;
        }

        public Value encode(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            return (Value) encodeMethod.invoke(codec, new Object[]{object});
        }

        public Object decode(Value value, Class<?> clazz) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            return decodeMethod.invoke(codec, new Object[]{value, clazz});
        }

        @Override
        public String toString() {
            return "JSONValueCodecHelper[codec=" + codec + " encodeMethod=" + encodeMethod + " decodeMethod=" + decodeMethod + "]";
        }
    }
}
