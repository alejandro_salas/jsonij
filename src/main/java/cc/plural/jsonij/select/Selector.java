/*
 * Copyright 2013 John.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.select;

/**
 *
 * @author John
 */
public class Selector {

    public static final Selector OBJECT_SELECTOR = new TypeObjectSelector();
    public static final Selector ARRAY_SELECTOR = new TypeArraySelector();
    public static final Selector NUMBER_SELECTOR = new TypeNumberSelector();
    public static final Selector STRING_SELECTOR = new TypeStringSelector();
    public static final Selector BOOLEAN_SELECTOR = new TypeBooleanSelector();
    public static final Selector NULL_SELECTOR = new TypeNullSelector();

    private static class TypeObjectSelector extends Selector {
    }

    private static class TypeArraySelector extends Selector {
    }

    private static class TypeNumberSelector extends Selector {
    }

    private static class TypeStringSelector extends Selector {
    }

    private static class TypeBooleanSelector extends Selector {
    }

    private static class TypeNullSelector extends Selector {
    }
}
