/*
 * Copyright 2013 John.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.select;


/**
 *
 * @author John
 */
public interface SelectConstants {

    public static final int COMBINATOR = ',';
    public static final String OBJECT = "object";
    public static final String ARRAY = "array";
    public static final String NUMBER = "number";
    public static final String STRING = "string";
    public static final String BOOLEAN = "boolean";
    public static final String NULL = "null";
    public static final int SELECTOR_SPLIT = '>';
    public static final int PSEUDO_KEY = ':';
    public static final int CLASS_KEY = '.';
    public static final int UNIVERSAL_KEY = '.';
    public static final int LEFT_PARENTHESIS = '(';
    public static final int RIGHT_PARENTHESIS = ')';
}
