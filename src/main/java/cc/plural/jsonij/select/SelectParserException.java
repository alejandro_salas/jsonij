/*
 * Copyright 2013 John.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.select;

import cc.plural.jsonij.parser.ParserException;
import java.lang.reflect.Array;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author John
 */
/**
 * Internationised Parser Exception.
 *
 * @author J.W.Marsden
 * @version 1.0.0
 */
public class SelectParserException extends ParserException {

    
    /**
     * Serial UID
     */
    private static final long serialVersionUID = 470719429874019122L;
    /**
     * Exception Key
     */
    protected String key;
    /**
     * Exception Locale
     */
    protected Locale locale;

    public static final String MESSAGE_BUNDLE = "SelectMessageBundle";

    
//    public SelectParserException(String key, Object... args) {
//        super(key, args);
//    }

    public SelectParserException(String key, int line, int position, Object... args) {
        super(key, line, position, args);
    }

    public SelectParserException(String key, int line, int position, Locale locale, Object... args) {
        super(key, line, position, locale, args);
    }

    public String getBundleName() {
        return MESSAGE_BUNDLE;
    }
}
