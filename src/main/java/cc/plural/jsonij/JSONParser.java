/*
 * Copyright 2012 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij;

import cc.plural.jsonij.parser.ParserException;
import java.io.IOException;
import java.io.Reader;

/**
 *
 * @author jmarsden
 */
public abstract class JSONParser {
    
    public enum Dialect {
        STRICT_RFC_4627,
        FLEXABLE_RFC_4627
    }
    
    protected Dialect dialect;
    
    public JSONParser() {
        dialect = null;
    }
    
    public Dialect getDialect() {
        return dialect;
    }
    
    public void setDialect(Dialect dialect) {
        this.dialect = dialect;
    }
    
    public abstract Value parse(java.lang.String targetString) throws IOException, ParserException;
   
    public abstract Value parse(Reader targetReader) throws IOException, ParserException;
}
